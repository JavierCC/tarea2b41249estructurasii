#include <iostream>
#include <string>
#include<vector>
using namespace std;
class Cache{
public:
    Cache();
    ~Cache();
    int cachesize;
    int linesize;
    int asociativity;
    int offsetsize;
    int tagsize;
    int setbits;
    int M;
    double overallmissrate;
    double readmissrate;
    int dirtyEvictions;
    int loadmiss;
    int storemiss;
    int loadhit;
    int storehit;
    int totalmiss;
    int totalhit;
    void calculos(int t, int l, int a);
    void leerInstrucciones();
    void imprimir();





};
